from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from celery import Celery
from elasticsearch import Elasticsearch
import os
from flask_babel import Babel
from config import Config


app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost'
app.config['SECRET_KEY'] = '5a89b9d4e4308549a4dafea4085403ae'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['ELASTICSEARCH_URL'] = 'http://localhost:9200'
ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
babel = Babel(app)
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])


db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'


celery = Celery(app.name, backend='redis://localhost', broker=app.config['CELERY_BROKER_URL'])

from quora_clone import routes