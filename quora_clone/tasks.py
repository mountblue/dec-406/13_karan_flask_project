from celery import Celery
from quora_clone.models import Answer, Question
import csv

celery_app = Celery('quora_clone_tasks', backend='redis://localhost', broker='redis://localhost')


@celery_app.task
def download_data_task(user_id):
    # business logic here. Remember to set appropriate path below.
    answers = Answer.query.filter(Answer.user_id==user_id).all()
    questions = Question.query.all()  
    data_list=[]  
    for answer in answers:
        temp_list=[]
        for question in questions:
            if question.id == answer.question_id:
                temp_list.append(question.content)
        temp_list.append(answer.content)
        data_list.append(temp_list) 
    filepath = 'quora_clone/user_data/'+str(user_id)+'.csv'
    with open(filepath, 'w') as data_file:
        fileWriter = csv.writer(data_file, delimiter=",")
        fileWriter.writerow(['Question', 'Answer'])
        for row in data_list:
            fileWriter.writerow(row)
    
    return {
        'status': 'Download completed',
        'filepath': 'user_data/{}.csv'.format(user_id)
    }
    