from flask_wtf import FlaskForm
from flask_login import  current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from quora_clone.models import User
from flask_babel import _, lazy_gettext as _l
from flask import request


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                            validators = [DataRequired(), Length(min=2, max=20)])

    email = StringField('Email',
                            validators = [DataRequired(), Email()])
    
    password = PasswordField('Password',
                            validators = [DataRequired()])
    
    confirm_password = PasswordField('Confirm Password',
                            validators = [DataRequired(), EqualTo('password')])
    
    submit = SubmitField('Sign up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Username is already taken')
    
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email is already in use')

class LoginForm(FlaskForm):
    email = StringField('Email',
                            validators = [DataRequired(), Email()])
    
    password = PasswordField('Password',
                            validators = [DataRequired()])
    remember = BooleanField('Remember Me')
    
    submit = SubmitField('Log in')


class UpdateAccountForm(FlaskForm):
    username = StringField('Username',
                            validators = [DataRequired(), Length(min=2, max=20)])

    email = StringField('Email',
                            validators = [DataRequired(), Email()])
    
    submit = SubmitField('Update')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('Username is already taken')
    
    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('Email is already in use')

class askQuestionForm(FlaskForm):
    content = TextAreaField('', validators=[DataRequired()])
    submit = SubmitField('Add')

class answerForm(FlaskForm):
    content = TextAreaField('', validators=[DataRequired()])
    submit = SubmitField('Submit')

class updateForm(FlaskForm):
    content = TextAreaField('', validators=[DataRequired()])
    submit = SubmitField('Update')

class SearchForm(FlaskForm):
   q = StringField(_l('Search'), validators=[DataRequired()])
   def __init__(self, *args, **kwargs):
       if 'formdata' not in kwargs:
           kwargs['formdata'] = request.args
       if 'csrf_enabled' not in kwargs:
           kwargs['csrf_enabled'] = False
       super(SearchForm, self).__init__(*args, **kwargs)

