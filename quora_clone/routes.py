from flask import current_app ,render_template, flash, redirect, url_for, request, send_file, jsonify
from quora_clone.models import User, Post, Question, Answer
from quora_clone.forms import RegistrationForm, LoginForm, UpdateAccountForm, askQuestionForm, answerForm, updateForm, SearchForm
from quora_clone import app, db, bcrypt, celery
from flask_login import  login_user, current_user, logout_user, login_required
import csv
from quora_clone.search import query_index, add_to_index, remove_from_index
from quora_clone.tasks import download_data_task
from flask import g
from flask_babel import get_locale,_


@app.route("/")
@app.route("/home")
def home():
    questions = Question.query.all()
    answers = Answer.query.all()
    return render_template('home.html', questions=questions, answers = answers)

@app.route("/register", methods = ['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username = form.username.data, email = form.email.data, password = hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(f'Account has been created, you can login now', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title= 'Register', form= form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email = form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember= form.remember.data)
            return redirect(url_for('home'))
        else:
            flash('Invalid Username or password')
    return render_template('login.html', title= 'Login', form= form)

@app.route("/blogs")
def blogs():
    return render_template('blog.html', title = "blog")

@app.route("/logout")
def logout():
    logout_user() 
    return redirect(url_for('home'))

@app.route("/account")
@login_required
def account():
    answers = Answer.query.filter(Answer.user_id==current_user.id).all()
    questions = Question.query.all()
    form = UpdateAccountForm()
    image_file = url_for('static', filename = 'profile_pics/' + current_user.image_file)
    return render_template("account.html", title= "account", image_file = image_file, form= form, answers= answers, questions=questions)

@app.route("/ask_question", methods=['GET', 'POST'])
@login_required
def ask_question():
    form = askQuestionForm()
    if form.validate_on_submit():
        question = Question(content = form.content.data, author = current_user)
        db.session.add(question)
        db.session.commit()
        flash('Your question has been added successfull!', 'success')
        return redirect(url_for('questions'))
    return render_template("ask_question.html", title="ask_question", form= form)

@app.route("/questions")
def questions():
    questions_list = Question.query.all()
    questions_list = questions_list[::-1]
    return render_template("questions.html", title="questions", questions = questions_list)


@app.route("/answer", methods=['GET', 'POST'])
@login_required
def answer():
    ans_form = updateForm()
    q_id = request.args.get('next')
    answers = Answer.query.filter(Answer.question_id == q_id).all()
    for answer in answers:
        if answer.user_id == current_user.id :
            ans_form.content.data = answer.content
            return render_template('update.html', title="update", answer = answer, form = ans_form)
    form = answerForm()
    if form.validate_on_submit():
        answer = Answer(content = form.content.data, author = current_user,question_id=q_id)
        db.session.add(answer)
        db.session.commit()
        flash('Your have successfully answered this question!', 'success')
        return redirect(url_for('home'))
    return render_template("answer.html", title="answer", form = form)

# @celery.task(bind = True)
# def file_download(user_id):
#     answers = Answer.query.filter(Answer.user_id==user_id).all()
#     questions = Question.query.all()  
#     data_list=[]  
#     for answer in answers:
#         temp_list=[]
#         for question in questions:
#             if question.id == answer.question_id:
#                 temp_list.append(question.content)
#         temp_list.append(answer.content)
#         data_list.append(temp_list)
#     filepath = 'quora_clone/data_'+str(user_id)+'.csv'
#     with open(filepath, 'w') as data_file:
#         fileWriter = csv.writer(data_file, delimiter=",")
#         fileWriter.writerow(['Question', 'Answer'])
#         for row in data_list:
#             fileWriter.writerow(row)
    
#     return {
#         'status': 'Download completed',   
#         'filepath': filepath
#     }


@app.route("/download", methods=['POST'])
def download():
    download = download_data_task.delay(current_user.id)
    return jsonify({}), 202, {'Location': url_for('download_status', download_id=download.id)}


@app.route('/download/<path:filepath>', methods=['GET'])
def download_data(filepath):
    return send_file(filepath)


@app.route('/downlad_status/<download_id>')
def download_status(download_id):
    download = download_data_task.AsyncResult(download_id)
    # print("ready=",download.ready())
    status = download.ready()
    if not status:
        return jsonify({
            'status': 'PENDING'
        })
    else:
        result = download.get()
        filepath = result['filepath']
        # redirect to '/download/<filepath>`
    if download.state == 'PENDING':
        response = {    
            'state': download.state,
            'status': 'Pending...'
        }
    elif download.state != 'FAILURE':
        response = {
            'state': download.state,
            'status': download.info.get('status', '')
        }
        if 'filepath' in download.info:
            response['filepath'] = download.info['filepath']
    else:
        response= {
            'state' : download.state,
            'status' : str(download.info)
        }
    return jsonify(response)


@app.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('home'))
    page = request.args.get('page', 1, type=int)
    posts, total = Question.search(g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])
    print(posts)
    print(total)
    return render_template('search.html', title='Search', posts= posts)

@app.before_request
def before_request():
   if current_user.is_authenticated:
       g.search_form = SearchForm()
   g.locale = str(get_locale())